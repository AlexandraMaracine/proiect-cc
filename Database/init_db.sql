CREATE TABLE IF NOT EXISTS games (
    id serial PRIMARY KEY,
    "name" varchar NOT NULL UNIQUE,
    "type" varchar NOT NULL,
    "description" varchar NOT NULL,
    "year" integer NOT NULL,
    "rating" float DEFAULT 0.0,
    "reviewers" integer DEFAULT 0
);

CREATE TABLE IF NOT EXISTS roles (
    id serial PRIMARY KEY,
    "value" varchar NOT NULL UNIQUE
);


CREATE TABLE IF NOT EXISTS users (
    id serial PRIMARY KEY,
    "username" varchar NOT NULL UNIQUE,
    "password" varchar NOT NULL,
    "role_id" integer REFERENCES roles(id)
);

INSERT INTO roles (value) VALUES ('ADMIN');
INSERT INTO roles (value) VALUES ('USER');
INSERT INTO users (username, password, role_id) VALUES ('admin', '$2y$10$BLMZFAnCPXX0cVRmdPP3Meu3NR/xWucAyQ4aAW2z57RlLdLPvH0Hi', 1);
INSERT INTO games (name, "type", description, "year", rating, reviewers) VALUES ('Exploding Kittens', 'Strategy', 'Exploding Kittens is a highly strategic kitty-powered version of Russian Roulette. Players take turns drawing cards until someone draws an exploding kitten and loses the game. The deck is made up of cards that let you avoid exploding by peeking at cards before you draw, forcing your opponent to draw multiple cards, or shuffling the deck.', 2015, 4.5, 14);
INSERT INTO games (name, "type", description, "year", rating, reviewers) VALUES ('Dark Stories', 'Verbal', 'In Dark Stories, one player is the game master who will read a scenario to the other players. The scenario almost always involves someone dying in an unusual way. The rest of the players must then try and deduce the specific scenario as to how the person died, by asking a series of yes/no questions.', 2015, 3.9, 10);
INSERT INTO games (name, "type", description, "year", rating, reviewers) VALUES ('Codenames', 'Verbal', 'It is a very simple team game, in which you use your imagination and creativity to win. Each team has a designated member who in one word must provide as many clues as possible to teammates.This is the adult version, which is more fun, but it is also the original version without blunt words. There are actually several variants in which the words differ, but there is also a variant with images.', 2014, 3.5, 9);
INSERT INTO games (name, "type", description, "year", rating, reviewers) VALUES ('Resistance', 'Logic', 'The Resistance is a party game of social deduction. It is designed for five to ten players, lasts about 30 minutes, and has no player elimination. The Resistance is inspired by Mafia/Werewolf, yet it is unique in its core mechanics, which increase the resources for informed decisions, intensify player interaction, and eliminate player elimination.', 2017, 2.0, 2);
INSERT INTO games (name, "type", description, "year", rating, reviewers) VALUES ('Alias', 'Verbal', 'Alias is a board game, where the objective of the players is to explain words to each other. Hence, Alias is similar to Taboo, but the only forbidden word in the explanations is the word to be explained. The game is played in teams of varying size, and fits well as a party game for larger crowds.', 2018, 4.6, 20);
INSERT INTO games (name, "type", description, "year", rating, reviewers) VALUES ('Jenga', 'Dexterity', 'It is extremely simple, you have to remove parts from the tower and put them on top without the tower collapsing.', 1986, 4.3, 30);
INSERT INTO games (name, "type", description, "year", rating, reviewers) VALUES ('Catan', 'Strategy', 'Players take on the roles of settlers, each attempting to build and develop holdings while trading and acquiring resources. Players gain victory points as their settlements grow; the first to reach a set number of victory points, typically 10, wins.', 1995, 4.6, 12);
INSERT INTO games (name, "type", description, "year", rating, reviewers) VALUES ('Pantomime', 'Mime', 'Ssssh! With hands and feet only, the players in Pantomime describe the pictures on the cards to other players. Whoever speaks or makes a noise loses.', 2003, 3.9, 16);
INSERT INTO games (name, "type", description, "year", rating, reviewers) VALUES ('WTF?', 'Verbal', 'WTF? is the fast-moving, quick-thinking, question and answer, bluff and banter game for broad-minded players. Nothing like your conventional fact-based quiz - in this game YOU decide the answers! Answer against the clock and, if you do not know the answer, try to convince others that you do!', 2017, 3.8, 21);