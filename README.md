Proiect-CC - Platforma pentru recomandari de boardgames

Autori:
Chirita Maria-Luissa - SSA
Maracine Nicoleta-Alexandra - SSA
Nemulescu Roxana-Elena - SSA

Aplicatia are scopul de a oferi utilizatorului mai multe posibilitati de a 
descoperi boardgameuri in functie de preferinte si popularitate.
Folosim o baza de date care contine un tabel cu diferite informatii despre
acestea: nume, tip, descriere, anul aparitiei, rating și numarul de note primite.
Utilizatorul poate sa vizualizeze toate jocurile, dar si sa filtreze jocuri in 
functie de tipul lor, anul aparitiei, vechime sau rating. În plus, exista si 
posibilitatea de a da rating unui joc.

Pentru a dezvolta aceasta aplicatie am creat mai multe microservicii:
    - API - serviciul prin care utilizatorul interactioneaza cu aplicatia
          - manipuleaza cererile care nu modifica baza de date
          - pune intr-o coada de mesaje comenzile care schimba baza de date
            pentru a fi trimise catre procesator
    - coada de mesaje - de tip rabbitmq
                      - primeste cererile de la API, iar procesatorul le preia
    - procesatorul - cand este disponibil, analizeaza mesajele din coada si executa
                     actiunile corespunzatoare in baza de date

De asemenea am mai folosit o baza de date Postgres pentru tabelele create și am utilizat
pgadmin pentru gestiunea ei.

Pentru orchestrator am folosit Docker Swarm. În docker-compose.yml am setat optiunile de
deploy ale servicilor din aplicatie. Pentru credentialele folosite am utilizat secrete,
iar pentru a testa am rulat pe un cluster cu un manager si doi workeri in cadrul siteului
https://labs.play-with-docker.com/

În plus, am folosit si Portainer pentru gestiunea clusterului.
