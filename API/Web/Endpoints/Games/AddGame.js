const express = require('express');

const AddGameCommandHandler = require('../../Commands/Games/Handlers/AddGameCommandHandler.js');
const {
    AddGameCommand
} = require ('../../Commands/Games/Models/AddGameCommand.js');

const Router = express.Router();

Router.post('/', async (req, res) => {
    
    const GameCommand = new AddGameCommand(req.body);
    try {
        AddGameCommandHandler.HandleAsync(GameCommand);
        res.status(201).end();
    } catch (err) {
        console.error(err);
        res.status(500).send("Something bad happened!");
    }
});

module.exports = Router;