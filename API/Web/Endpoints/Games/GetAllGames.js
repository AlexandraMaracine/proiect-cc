const express = require('express');

const GetAllGamesQueryHandler = require('../../Queries/Games/Handlers/GetAllGamesQueryHandler.js');
const GetGamesByTypeQueryHandler = require('../../Queries/Games/Handlers/GetGamesByTypeQueryHandler.js');
const GetGamesByYearQueryHandler = require('../../Queries/Games/Handlers/GetGamesByYearQueryHandler.js')
const GetRecentGamesQueryHandler = require('../../Queries/Games/Handlers/GetRecentGamesQueryHandler.js')
const GetTopRatedGamesQueryHandler = require('../../Queries/Games/Handlers/GetTopRatedGamesQueryHandler.js');

const Router = express.Router();

Router.get('/', async (req, res) => {
    
    try {
        var games = await GetAllGamesQueryHandler.QueryAsync();
        res.json(games);
    } catch (err) {
        console.error(err);
        res.status(500).send("Something bad happened!");
    }
});

Router.get('/byType/:type', async (req, res) => {
    
    try {
        var type = req.params.type;
        var games = await GetGamesByTypeQueryHandler.QueryAsync(type);
        res.json(games);
    } catch (err) {
        console.error(err);
        res.status(500).send("Something bad happened!");
    }
});

Router.get('/byYear/:year', async (req, res) => {
    
    try {
        var year = req.params.year;
        var games = await GetGamesByYearQueryHandler.QueryAsync(year);
        res.json(games);
    } catch (err) {
        console.error(err);
        res.status(500).send("Something bad happened!");
    }
});

Router.get('/recent/:year', async (req, res) => {
    
    try {
        var year = req.params.year;
        var games = await GetRecentGamesQueryHandler.QueryAsync(year);
        res.json(games);
    } catch (err) {
        console.error(err);
        res.status(500).send("Something bad happened!");
    }
});

Router.get('/rating', async (req, res) => {
    
    try {
        var games = await GetTopRatedGamesQueryHandler.QueryAsync();
        res.json(games);
    } catch (err) {
        console.error(err);
        res.status(500).send("Something bad happened!");
    }
});

module.exports = Router;