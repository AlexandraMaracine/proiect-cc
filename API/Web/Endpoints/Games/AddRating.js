const express = require('express');

const AddRatingCommandHandler = require('../../Commands/Games/Handlers/AddRatingCommandHandler.js');
const {
    AddRatingCommand
} = require ('../../Commands/Games/Models/AddRatingCommand.js');

const Router = express.Router();

Router.put('/:name', async (req, res) => {
    
    const RatingCommand = new AddRatingCommand(req.params.name, req.body.rating);
    try {
        AddRatingCommandHandler.HandleAsync(RatingCommand);
        res.status(200).end();
    } catch (err) {
        console.error(err);
        res.status(500).send("Something bad happened!");
    }
});

module.exports = Router;