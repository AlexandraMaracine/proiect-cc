const express = require('express');

const AddGamesEndpoint = require('./AddGame');
const GetGamesEndpoint = require('./GetAllGames');
const AddRatingEndpoint = require('./AddRating');

const Router = express.Router();

Router.use('/', AddGamesEndpoint);
Router.use('/', GetGamesEndpoint);
Router.use('/', AddRatingEndpoint);

module.exports = Router;