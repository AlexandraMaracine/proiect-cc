const express = require('express');

const GamesEndpoint = require('./Games');

const Router = express.Router();

Router.use('/games', GamesEndpoint);

module.exports = Router;