const MessageQueue = require('../../../../Infrastructure/RabbitMQ');
const { AddRatingCommand } = require('../Models/AddRatingCommand');

const QUEUE = 'games';

/**
 * 
 * @param {AddRatingCommand} ratingCommand 
 */
const HandleAsync = async (ratingCommand) => {
    const payload = {
        type: "rating",
        payload: ratingCommand
    }
    await MessageQueue.PublishMessageAsync(QUEUE, payload);
};

module.exports = {
    HandleAsync
}