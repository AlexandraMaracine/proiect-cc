const MessageQueue = require('../../../../Infrastructure/RabbitMQ');

const QUEUE = 'games';

/**
 * 
 * @param {AddGameCommand} gameCommand 
 */
const HandleAsync = async (gameCommand) => {
    const payload = {
        type: "game",
        payload: gameCommand
    }
    await MessageQueue.PublishMessageAsync(QUEUE, payload);
};

module.exports = {
    HandleAsync
}