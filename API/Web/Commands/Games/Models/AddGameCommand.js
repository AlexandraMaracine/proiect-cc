class AddGameCommand {
    constructor (entry) {
        this.name = entry.name;
        this.type = entry.type;
        this.description = entry.description;
        this.year = entry.year;
    }
}

module.exports = {
    AddGameCommand
}