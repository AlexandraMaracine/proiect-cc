const {
    ExecuteQuery
} = require('../../../../Infrastructure/Database');

const GamesView = require('../Models/GamesView.js');

const QueryAsync = async (type) => {
    
    const result = await ExecuteQuery('SELECT * FROM games WHERE type = $1', [type]);

    return result.map(x => new GamesView(x));
}

module.exports = {
    QueryAsync
}