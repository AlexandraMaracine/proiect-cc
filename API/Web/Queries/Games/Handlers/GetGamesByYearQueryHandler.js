const {
    ExecuteQuery
} = require('../../../../Infrastructure/Database');

const GamesView = require('../Models/GamesView.js');

const QueryAsync = async (year) => {
    
    const result = await ExecuteQuery('SELECT * FROM games WHERE year = $1', [year]);

    return result.map(x => new GamesView(x));
}

module.exports = {
    QueryAsync
}