const {
    ExecuteQuery
} = require('../../../../Infrastructure/Database');

const GamesView = require('../Models/GamesView.js');

const QueryAsync = async () => {
    const result = await ExecuteQuery('SELECT * FROM games ORDER BY rating DESC');

    return result.map(x => new GamesView(x));
}

module.exports = {
    QueryAsync
}