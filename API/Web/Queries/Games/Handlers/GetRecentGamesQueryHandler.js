const {
    ExecuteQuery
} = require('../../../../Infrastructure/Database');

const GamesView = require('../Models/GamesView.js');

const QueryAsync = async (noOfYears) => {
    var currentTime = new Date();
    const year = currentTime.getFullYear() - noOfYears;
    const result = await ExecuteQuery('SELECT * FROM games WHERE year >= $1 ORDER BY year DESC', [year]);

    return result.map(x => new GamesView(x));
}

module.exports = {
    QueryAsync
}