class GamesView {
    constructor (entry) {
        this.name = entry.name;
        this.type = entry.type;
        this.description = entry.description;
        this.year = entry.year;
        this.rating = entry.rating;
        this.reviewers = entry.reviewers;
    }
}

module.exports = GamesView;