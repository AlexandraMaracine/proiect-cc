const { Pool } = require('pg')

const {
  getSecret
} = require('docker-secret');

const options = {
  host: process.env.PGHOST,
  database: process.env.PGDATABASE,
  port: process.env.PGPORT,
  user: process.env.NODE_ENV === 'development' ? process.env.PGUSER : getSecret(process.env.PGUSER_FILE),
  password: process.env.NODE_ENV === 'development' ? process.env.PGPASSWORD : getSecret(process.env.PGPASSWORD_FILE)
}

const pool = new Pool(options);

pool.on('error', (err, client) => {
    console.error('Unexpected error on idle client', err)
    process.exit(-1)
  });

const ExecuteQuery = async (text, params) => {
    const {
      rows,
    } = await pool.query(text, params);

    return rows;
};

const CalculateRatingAndExecuteQuery = async (name, givenRating) => {
  const game = await pool.query("SELECT rating, reviewers FROM games WHERE name=$1", [name]);

  let rating = game.rows[0].rating;
  let reviewers = game.rows[0].reviewers;

  let newRating = ((rating * reviewers + givenRating) / (++reviewers)).toFixed(1);

  const {
    rows,
  } = await pool.query("UPDATE games SET rating=$1, reviewers=$2 WHERE name=$3", [newRating, reviewers, name]);

  return rows;
};

module.exports = {
    ExecuteQuery,
    CalculateRatingAndExecuteQuery
}