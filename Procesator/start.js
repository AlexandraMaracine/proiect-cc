require('dotenv').config()

var amqp = require('amqplib/callback_api');

const {
    ExecuteQuery,
    CalculateRatingAndExecuteQuery
} = require('./Database');

const QUEUE = 'games';

amqp.connect(process.env.AMQPURL, (error0, connection) => {
    if (error0) {
        throw error0;
    }
    connection.createChannel((error1, channel) => {
        if (error1) {
            throw error1;
        }

        channel.assertQueue(QUEUE, {
            durable: false
        });

        console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", QUEUE);

        channel.consume(QUEUE, async (msg) => {
            console.log(" [x] Received %s", msg.content.toString());

            if (msg !== null) {
                const content = JSON.parse(msg.content);
                const payload = content.payload;
                try {
                    if (content.type == "game") {
                        await ExecuteQuery("INSERT INTO games (name, type, description, year) VALUES ($1, $2, $3, $4)", [payload.name, payload.type, payload.description, payload.year]);
                    } else if (content.type == "rating") {
                        await CalculateRatingAndExecuteQuery(payload.name, payload.rating);
                    }
                } catch (err) {
                    console.error(err);
                }
            }

            channel.ack(msg);
        });
    });
});